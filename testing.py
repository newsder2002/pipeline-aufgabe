""" Testcase for moodle_ispector.py """
import unittest
from moodle_inspector import userdatamanager, sesskeyfinder


class Testinspector(unittest.TestCase):
    """ Best Class pylint... Best. finest. Class. """
    def test_string(self):
        """ Dict Test """
        name = "Hallo"
        passw = "Heiko"
        credantials = userdatamanager(name, passw)
        lenght = len(credantials)
        # return 3 items?
        self.assertEqual(3, lenght)
        # Komplett?
        self.assertEqual(credantials['username'], 'Hallo')
        self.assertEqual(credantials['password'], 'Heiko')

    def test_sesskey(self):
        """ Test tokenlenght """
        key = sesskeyfinder("foo", "bar")
        lenght = len(key)
        self.assertEqual(10, lenght)


if __name__ == '__main__':
    unittest.main(verbosity=1)
