""" Connect to Moodle and get the token to take the sesskey """
__author__ = 'Benjamin B'
import re
import requests
from bs4 import BeautifulSoup

session = requests.session()


def userdatamanager(username, password):
    """ Take the Token from Moodle """
    moodle = session.get('https://moodle.itech-bs14.de/')
    loginpage = BeautifulSoup(moodle.text, 'html.parser')
    token = loginpage.find("input", attrs={"name": "logintoken"})["value"]
    return {"username": username, "password": password, "logintoken": token}


def sesskeyfinder(username, password):
    """ Login with token and take the sesskey """
    sesskey = ""
    login = session.post("https://moodle.itech-bs14.de/login/index.php", data=userdatamanager(
        username,
        password))
    mainpage = BeautifulSoup(login.text, "html.parser")
    regex = re.compile(r"\"sesskey\":\"[^@]+@[^@]", re.MULTILINE | re.DOTALL)
    findsesskey = mainpage.find("script", text=regex)
    for items in findsesskey:
        result = regex.search(str(items))
        sesskey = result.string.split("\"sesskey\":\"")[-1].split("\"")[0]
    return sesskey


if __name__ == '__main__':
    key = sesskeyfinder("foo", "bar")
    print(key)
    session.close()

# sessid = ???
